== Extension permanent link
Permanent link: http://shop.etwebsolutions.com/eng/et-reviewnotify.html
Support link: http://support.etwebsolutions.com/projects/et-reviewnotify/issues/new

== Short Description
Extension notifies store owner about new reviews
and complicates automatic review posting for spam bots.

== Version Compatibility
Magento CE:
1.3.х (tested in 1.3.2.4)
1.4.x (tested in 1.4.1.1)
1.5.x (tested in 1.5.0.1 and 1.5.1.0)
1.6.x (tested in 1.6.1.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.0.0)
1.9.x (tested in 1.9.2.2)

== Installation
http://doc.etwebsolutions.com/en/instruction/reviewnotify/install

== Information for Developers
http://doc.etwebsolutions.com/en/instruction/reviewnotify/technical-information-for-the-developer